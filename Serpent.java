import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.JPanel;


/**
 * Creation d'un classe Serpent hérité de Jpanel
 * @author Moi
 */
public class Serpent extends JPanel {
    public int longueur;
    Graphics g;
    public boolean jouer=true; 
    public int nbrDePommes = 3;
    Scanner sc;
    ArrayList<Anneau> corps = new ArrayList<>(); // un tableau de type Anneau
    ArrayList<Pomme> pommes = new ArrayList<>(); // un tableau de typr Pomme
    
    public int direction = 4;
    
    // Constructeur Serpent
    public Serpent(Graphics g, int longueur){
        this.g = g;
        this.longueur=longueur;
        sc = new Scanner(System.in);
        
    }
    public void jouer()
    {
        creationSerpent();
        
        while(jouer==true)
        {
            g.setColor(Color.LIGHT_GRAY);
            g.fillRect(0, 0, Fenetre.largeurFen, Fenetre.hauteurFen);
            
            creationPommes();
            dessinePommes();
            dessineSerpent();
            
            sleep(200);
            
            move();
            checkCollision();
        }
    }
    public void sleep (int time)
    {
        try{
            Thread.sleep(time);
            }catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
    }
    // methode pour crée des Pommes 
    public void creationPommes(){
        int randX, randY;
        Boolean creation = true;
        while(pommes.size()< nbrDePommes)
        {
            creation = true;
            int widthFen = ((Fenetre.largeurFen-20)/10)-2; // largeur Fenetre  on divise par 10 pour que le pomme 
            int heightFen = ((Fenetre.hauteurFen-20)/10)-2; // hauteur de Fenetre  n'apparaisse n'importe ou
            
            randX = (int)((Math.random()*(widthFen))+3); // nombre hasard tiré por la position X
            randY = (int)((Math.random()*(heightFen))+3); // nomnvre hasard tiré pour la position Y
            
            randX = (randX*10); // on miltiplie pour retrouvé la valeur de départ
            randY = (randY*10);
            
            // on doit assuré que les pommes n'apparaisse pas sur le corps du serpent
            for(int i=0; i<corps.size(); i++)
            {
                Anneau siExiste = corps.get(i);
                
                if (randX == siExiste.posX && randY == siExiste.posY )
                {
                    creation = false;
                }
            }
            if(creation == true)
            {
               pommes.add(new Pomme(randX,randY,Color.GREEN));
            }
        }
    }
    // methode pour crée le Serpent 
     public void creationSerpent()
     {
         for(int j=0; j<this.longueur;j++)
         {
           int hauteur;
           hauteur = ((int) Fenetre.hauteurFen/2)/10;
           hauteur*=10;
           if(j==0)
           {
               corps.add(new Anneau(Fenetre.largeurFen/2+((j)*10),hauteur,Color.RED)); //LA téte du Serpent rouge
           }
           else if(j > 0)
               corps.add(new Anneau(Fenetre.largeurFen/2+((j)*10),hauteur,Color.BLACK)); // le reste du corps de Serpent Noir
      }
     }
      public void dessineSerpent()
          {
              for(int i=0; i< corps.size(); i++)
              {
                  Anneau a;
                  a = corps.get(i);
                  
                  g.setColor(a.couleur);
                  g.fillOval(a.posX, a.posY, 10, 10);
                   showScore();
              }
    }
          public void showScore()
         {
             g.setFont(new Font("TimesRomam", Font.PLAIN,15));
             g.drawString(Integer.toString(corps.size()), 10, Fenetre.hauteurFen-10);
         }
     public void checkCollision()
     {
     // collision Avec Pomme
     for(int i=0; i<pommes.size(); i++)
     {
         Pomme checkPomme = pommes.get(i);
         Anneau checkSerpent = corps.get(0); 
         Anneau lastPosition = corps.get(corps.size()-1);
         
         if(checkPomme.posX== checkSerpent.posX && checkPomme.posY == checkSerpent.posY)
         {
             pommes.remove(i);
             corps.add(new Anneau(200+((lastPosition.posX)+10),0,Color.BLACK));
         }
     }   
     // Collision avec  Serpent
     for(int i=1; i<corps.size(); i++)
     {
         
         Anneau corpsSerpent= corps.get(i);
         Anneau teteSerpent = corps.get(0);
         
         if(teteSerpent.posX==corpsSerpent.posX && teteSerpent.posY==corpsSerpent.posY)
         
            jouer= false;
            
         
     }
     //  Si Anneau depasse fenetre 
     Anneau teteSerpent = corps.get(0);
     
     if(teteSerpent.posX<10)
         jouer=false;
     if(teteSerpent.posX > (Fenetre.largeurFen-20))
         jouer=false;
     if(teteSerpent.posY < 30)
         jouer = false;
     if(teteSerpent.posY > (Fenetre.hauteurFen-20))
         jouer=false;
     
     }
         public void dessinePommes()
         {
             for(int x=0; x<pommes.size(); x++)
             {
                 Pomme p = pommes.get(x);
                 
                 g.setColor(p.couleur);
                 g.fillOval(p.posX, p.posY, 10, 10);
                
             }
         }
         
         
         public void move()
         {
             int px,py;
             for(int i=corps.size()-1; i>0 ;i--)
             {
                 Anneau a;
                 a=corps.get(i-1);
                 px= a.posX;
                 py= a.posY;
                 a = corps.get(i);
                 a.posX = px;
                 a.posY = py;
             }
             Anneau b1= corps.get(0);
             if(direction==1)
                 b1.posY -= 10;
             if(direction ==2)
                 b1.posX +=10;
             if(direction==3)
                 b1.posY += 10;
             if(direction==4)
                 b1.posX -=10;
         }
}


import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
/**
 *classe qui va répresenté le fenetre de jeux
 * @author Moi
 */
public class Fenetre extends JFrame implements KeyListener{ // on implante un Keylistener pour mettre nos boutons sur écoute
        JPanel container = new JPanel(); // Instanciation d'un objet JPanel
        public static int largeurFen=200; // largeur fénetre
        public static int hauteurFen= 150; // hauteur fenetre
        Serpent anaconda;
        JLabel gameOver= new JLabel();
        
       // creation de Constructeur Fenetre
        public Fenetre(){
        this.setTitle("snake");// titre du jeu
        this.setSize(largeurFen,hauteurFen);// définit la taille: 200 pixels de large et 150 pixels de haut
        this.setResizable(false);  // crée un fenetre 
        this.setLocationRelativeTo(null);// nous demendons à notre objet de se positionner au centre
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);// Terminer le pocessus le prcessus lorsqu'on clique sur la croix rouge
        container.setBackground(Color.LIGHT_GRAY);// définit la couleur de Fond
        
        gameOver.setBounds((largeurFen/2)-40,0,100,20);
        
        this.addKeyListener(this); // Pour savoir bouton sur le clavier que j'ai appui
        
        // on prévient notre JFrame que notre JPanel sera son content container
        this.setContentPane(container);
        this.setVisible(true);
        
        anaconda= new Serpent(this.getGraphics(),4);
        
        
    }
    public void starGame()
    {
        anaconda.jouer();
        gameOver();
    }
    public void gameOver()
    {
        gameOver.setText("Game Over !");
        container.add(gameOver);
        container.repaint();
    }


    @Override
    public void keyTyped(KeyEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyPressed(KeyEvent e) {
       if(e.getKeyCode() == KeyEvent.VK_UP && anaconda.direction !=3)
           anaconda.direction = 1;
       if(e.getKeyCode() == KeyEvent.VK_RIGHT && anaconda.direction !=4)
           anaconda.direction =2;
       if(e.getKeyCode() == KeyEvent.VK_DOWN && anaconda.direction !=1)
           anaconda.direction = 3;
       if(e.getKeyCode() == KeyEvent.VK_LEFT && anaconda.direction !=2)
           anaconda.direction = 4;
    }

    @Override
    public void keyReleased(KeyEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

import java.awt.Color;
/**
 * Creation d'un classe Anneau
 * @author Moi
 */
public class Anneau {
    public int posX,posY;
    Color couleur;
    // Constructeur Anneau
    public Anneau(int x, int y, Color couleur){
        this.posX = x;
        this.posY = y;
        this.couleur = couleur;
        
    }
    
}
